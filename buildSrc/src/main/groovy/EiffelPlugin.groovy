/*
MIT License
Copyright (c) 2019 Michele Maione
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
import org.gradle.api.Plugin
import org.gradle.api.Project

class EiffelConfig
{
    def ISE_EIFFEL = (System.getenv()['ISE_EIFFEL'] ? System.getenv()['ISE_EIFFEL'] : '/opt/Eiffel_16.05')
    def ISE_PLATFORM = (System.getenv()['ISE_PLATFORM'] ? System.getenv()['ISE_PLATFORM'] : 'linux-x86-64')
    def ISE_EIFFEL_BIN = ISE_EIFFEL + '/studio/spec/' + ISE_PLATFORM + '/bin'
    def targetcfg
    def target
}

class EiffelPlugin implements Plugin<Project>
{
    void apply(Project project)
    {
        project.extensions.create("eiffel", EiffelConfig    )

        project.task('melt') << {
            if (!project.buildDir.exists())
                project.buildDir.mkdirs()

            project.exec {
                environment 'ISE_EIFFEL', project.eiffel.ISE_EIFFEL
                environment 'ISE_PLATFORM', project.eiffel.ISE_PLATFORM
                workingDir 'src'
                commandLine project.eiffel.ISE_EIFFEL_BIN + '/ec', '-project_path', "$project.buildDir", '-batch', '-config', project.eiffel.targetcfg, '-finalize', '-c_compile'
            }
        }

        project.task('finish_freezing') << {
            project.exec {
                environment 'ISE_EIFFEL', project.eiffel.ISE_EIFFEL
                environment 'ISE_PLATFORM', project.eiffel.ISE_PLATFORM

                workingDir "$project.buildDir" + '/EIFGENs/' + project.eiffel.target + '/W_code'

                commandLine project.eiffel.ISE_EIFFEL_BIN + '/finish_freezing'
            }

            project.copy {
                from "$project.buildDir" + '/EIFGENs/' + project.eiffel.target + '/W_code'
                into "$project.buildDir"
                include "${project.eiffel.target}"
            }
        }

        project.task('cleanEiffel') << {
            project.buildDir.deleteDir()
        }

        project.tasks.getByName('finish_freezing').dependsOn(project.tasks.getByName('melt'))
    }
}